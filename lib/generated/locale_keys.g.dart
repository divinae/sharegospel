// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

abstract class  LocaleKeys {
  static const ok = 'ok';
  static const yes = 'yes';
  static const no = 'no';
  static const add = 'add';
  static const cancel = 'cancel';
  static const showMore = 'showMore';
  static const chooseYourLanguage = 'chooseYourLanguage';
  static const changeLanguage = 'changeLanguage';
  static const english = 'english';
  static const polish = 'polish';
  static const russian = 'russian';
  static const noInternet = 'noInternet';
  static const homePage = 'homePage';
  static const personalStatsPage = 'personalStatsPage';
  static const gospelPage = 'gospelPage';
  static const chooseCity = 'chooseCity';
  static const addNewCity = 'addNewCity';
  static const beenSaved = 'beenSaved';
  static const yourCity = 'yourCity';
  static const cityCount = 'cityCount';
  static const yourCount = 'yourCount';
  static const gospelHeard = 'gospelHeard';
  static const gospelHeardStats = 'gospelHeardStats';
  static const inviteJesus = 'inviteJesus';
  static const inviteJesusStats = 'inviteJesusStats';
  static const notInviteJesusStats = 'notInviteJesusStats';
  static const person = 'person';
  static const persons = 'persons';
  static const personsSec = 'personsSec';
  static const howToShare = 'howToShare';
  static const howToShareLink = 'howToShareLink';
  static const gospelImagePath = 'gospelImagePath';
  static const gospelText = 'gospelText';
  static const january = 'january';
  static const february = 'february';
  static const march = 'march';
  static const april = 'april';
  static const may = 'may';
  static const june = 'june';
  static const july = 'july';
  static const august = 'august';
  static const september = 'september';
  static const october = 'october';
  static const november = 'november';
  static const december = 'december';
  static const yearlyPlot = 'yearlyPlot';
  static const monthlyPlot = 'monthlyPlot';

}
