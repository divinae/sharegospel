// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'personal_shares.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PersonalSharesAdapter extends TypeAdapter<PersonalShares> {
  @override
  final int typeId = 1;

  @override
  PersonalShares read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return PersonalShares()
      ..shares = (fields[0] as Map)?.cast<String, int>()
      ..saved = (fields[1] as Map)?.cast<String, int>();
  }

  @override
  void write(BinaryWriter writer, PersonalShares obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.shares)
      ..writeByte(1)
      ..write(obj.saved);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PersonalSharesAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
