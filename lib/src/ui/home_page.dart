import 'package:connectivity/connectivity.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sharegospel/src/bloc/add_share_bloc.dart';
import 'package:sharegospel/src/bloc/user_data_bloc.dart';
import 'package:sharegospel/src/bloc/user_data_event.dart';
import 'package:sharegospel/src/ui/data/user_data.dart';
import 'package:sharegospel/src/utils/dialogs_helper.dart';
import 'package:sharegospel/src/utils/logger.dart';
import 'package:transparent_image/transparent_image.dart';
import 'data/add_share_data.dart';
import '../utils/analytics_helper.dart';
import 'fragments/gospel_fragment.dart';
import 'fragments/home_page_fragment.dart';
import '../resources/consts.dart';
import 'package:sharegospel/generated/locale_keys.g.dart';

class DrawerItem {
  String title;
  IconData icon;

  DrawerItem(this.title, this.icon);
}

class HomePage extends StatefulWidget {
  HomePage({Key key, this.analytics}) : super(key: key);

  final FirebaseAnalytics analytics;

  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<HomePage> with WidgetsBindingObserver {
  final _pageViewController = PageController();
  int _activePage = 0;
  ConnectivityResult connectionResult;
  var _subscription;
  UserData _userData;

  String _getPageTitle(int index) {
    switch (index) {
      case 0:
        return LocaleKeys.homePage.tr();
      case 1:
        return LocaleKeys.gospelPage.tr();
    }

    return "";
  }

  final List<Widget> _fragments = [
    HomePageFragment(),
    GospelFragment(),
  ];

  @override
  void initState() {
    super.initState();
    logger.d("initState");
    WidgetsBinding.instance.addObserver(this);
    Connectivity().checkConnectivity().then((val) => connectionResult = val);
    _subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      logger.d("internet state changed state=" + result.toString());
      setState(() {
        connectionResult = result;
      });
    });
  }

  @override
  void dispose() {
    _pageViewController.dispose();
    _subscription.cancel();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    if (state == AppLifecycleState.resumed) {
      connectionResult = await (Connectivity().checkConnectivity());
      logger.d("resume " + connectionResult.toString());
      setState(() {});
    }
  }

  void _handleClick(BuildContext context, String value) {
    logger.d(value);
    switch (value) {
      case CHANGE_LANGUAGE_KEY:
        DialogHelper.chooseLanguage(context);
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    var _screenWidth = MediaQuery.of(context).size.width;
    logger.d("build home page");

    return Scaffold(
      appBar: AppBar(
        backgroundColor:
            MediaQuery.of(context).platformBrightness == Brightness.dark
                ? Colors.black26
                : Colors.white,
        title: Text(_getPageTitle(_activePage)),
        actions: <Widget>[
          PopupMenuButton<String>(
            onSelected: (value) => _handleClick(context, value),
            itemBuilder: (BuildContext context) {
              return [
                PopupMenuItem<String>(
                  value: CHANGE_LANGUAGE_KEY,
                  child: Text(LocaleKeys.changeLanguage.tr()),
                )
              ];
            },
          ),
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: _activePage == 0
          ? FloatingActionButton(
              foregroundColor:
                  MediaQuery.of(context).platformBrightness == Brightness.dark
                      ? Colors.black
                      : Colors.white,
              onPressed: () {
                DialogHelper.addNewShare(context);
              },
              tooltip: LocaleKeys.add.tr(),
              child: Icon(Icons.add),
              elevation: 2.0,
            )
          : null,
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        clipBehavior: Clip.antiAlias,
        child: BottomNavigationBar(
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: LocaleKeys.homePage.tr(),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.mood),
              label: LocaleKeys.gospelPage.tr(),
            )
          ],
          currentIndex: _activePage,
          backgroundColor:
              MediaQuery.of(context).platformBrightness == Brightness.dark
                  ? Colors.black45
                  : Colors.white,
          onTap: (index) {
            _pageViewController.animateToPage(index,
                duration: Duration(milliseconds: 300), curve: Curves.easeOut);
          },
        ),
      ),
      body: connectionResult == ConnectivityResult.none
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  FadeInImage(
                    placeholder: MemoryImage(kTransparentImage),
                    image: AssetImage(NO_INTERNET_ASSET_PATH),
                    height: 150,
                  ),
                  Padding(padding: const EdgeInsets.symmetric(vertical: 8.0)),
                  Text(LocaleKeys.noInternet.tr(),
                      style: Theme.of(context).textTheme.subtitle1)
                ],
              ),
            )
          : Center(
              child: Container(
                width: kIsWeb
                    ? _screenWidth > MAX_SCREEN_WIDTH
                        ? MAX_SCREEN_WIDTH
                        : _screenWidth
                    : _screenWidth,
                child: BlocProvider(
                  create: (BuildContext context) =>
                      UserDataBloc()..add(InitUserDataEvent()),
                  child: MultiBlocListener(
                    listeners: [
                      BlocListener<UserDataBloc, UserData>(
                        listener: (context, userData) {
                          if (userData != null) {
                            _userData = userData;
                          }
                        },
                      ),
                      BlocListener<AddShareBloc, AddShareData>(
                        listener: (context, addShareData) {
                          if (_userData != null) {
                            AnalyticsHelper.sendAddShareEvent(widget.analytics,
                                _userData, addShareData.saved);
                          }
                        },
                      ),
                    ],
                    child: PageView(
                      controller: _pageViewController,
                      children: _fragments,
                      onPageChanged: (index) {
                        setState(() {
                          _activePage = index;
                        });
                      },
                    ),
                  ),
                ),
              ),
            ),
    );
  }
}
